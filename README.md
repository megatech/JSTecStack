>js的技术体系


+  原生js
    - h5
    - web
    - 小程序
+  es5/es6
    - babel :编译为原生js
    - webpack:打包部署web工程
+  electronjs
    - typescript
    - pc应用
+  react/react-native
    - react技术体系
    - 跨平台原生app应用
+  nodejs/异步服务器框架
    - eggJS:阿里技术体系搭配antdesign使用,基于koa
    - express: koa的父亲,经典mvc框架，rest规范
+  爬虫
    - phantomjs：无头浏览器
    - 